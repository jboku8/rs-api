create table `invoices`
  (
     `ID`            int unsigned not null auto_increment primary key,
     `SELLER_UN_ID`  int not null,
     `SEQ_NUM_B`     varchar(255) not null,
     `STATUS`        int not null,
     `WAS_REF`       int not null,
     `F_SERIES`      varchar(255) not null,
     `F_NUMBER`      varchar(255) not null,
     `REG_DT`        datetime not null,
     `OPERATION_DT`  datetime not null,
     `S_USER_ID`     int not null,
     `B_S_USER_ID`   int not null,
     `DOC_MOS_NOM_B` varchar(255) not null,
     `SA_IDENT_NO`   varchar(255) not null,
     `ORG_NAME`      varchar(255) not null,
     `NOTES`         varchar(255) not null,
     `TANXA`         varchar(255) not null,
     `VAT`           int not null,
     `K_ID`          int not null,
     `AGREE_DATE`    datetime not null
  )
default character set utf8
collate utf8_unicode_ci