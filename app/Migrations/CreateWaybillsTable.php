<?php 

namespace RS\Migrations;

use RS\Models\Database;

/**
* Class for creating waybills table
*/
class CreateWaybillsTable
{
	const name = 'Create waybills table';

	public function Migrate()
	{
		$waybills_table = file_get_contents(__dir__ . '/Sql/waybills.sql');
		$index = file_get_contents(__dir__ . '/Sql/waybills_index.sql');
		$db = new Database();
		$res = $db->query($waybills_table);

		if(!$res){
			throw new Exception("Error migrating table");
		}
		$db->query($index);

		$db->close();
		return true;
	}
}