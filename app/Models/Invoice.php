<?php

namespace RS\Models;

use RS\Config\Config;
use RS\Models\Database;

/**
* Invoice Model Class
*/
class Invoice
{
	public $fields = ["ID", "SELLER_UN_ID", "SEQ_NUM_B", "STATUS", "WAS_REF", "F_SERIES", "F_NUMBER", "REG_DT", "OPERATION_DT", "S_USER_ID", "B_S_USER_ID", "DOC_MOS_NOM_B", "SA_IDENT_NO", "ORG_NAME", "NOTES", "TANXA", "VAT", "K_ID", "AGREE_DATE"];

	public $table = 'invoices';

	function __construct($invObject)
	{
		foreach ($this->fields as $key => $field) {
			if(isset($invObject->$field)){
				$this->$field = $invObject->$field;
			}
		}
	}

	public function display()
	{
		foreach ($this->fields as $key => $field) {
			if(isset($this->$field)){
				echo $field . ": " .$this->$field . "\r\n";
			}
		}
	}
}