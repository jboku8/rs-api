<?php

namespace RS\Services;
use RS\Config\Config;


/**
 * rs.ge api PHP implementation for waybils 
 */
class RS
{
	/**
     * @var string დეკლარირების მომხმარებელი
	 */
	protected $user; 

	/**
     * @var string დეკლარირების პაროლი
	 */
	protected $password;

	/**
	 * @var string საიდენტიფიკაციო ნომერი 
	 */
	protected $tin;

	/**
     * @var string სერვისის მომხმარებელი
	 */
	protected $su;

	/**
     * @var string სერვისის პაროლი
	 */
	protected $sp;

	/**
     * @var string მომხმარებლის IP
	 */
	protected $ip;

	/**
     * @var string WEB სერვისის მისამართი
	 */
	protected $uri;

	/**
     * @var string SOAP client object 
	 */
	protected $client;
	
	function __construct()
	{
		$this->user = Config::USER_NAME;
		$this->password = Config::USER_PASSWORD;
		$this->tin = Config::TIN;
		$this->su = Config::SERVICE_USER;
		$this->sp = Config::SERVICE_PASSWORD;
		$this->ip = Config::IP_ADDRESS;
	}

	public function parseResult($xmlData)
	{
		$data = simplexml_load_string($xmlData);
		// $data = json_decode(json_encode($data));
		return $data;
	}
}