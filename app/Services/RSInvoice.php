<?php 

namespace RS\Services;

use RS\Config\Config;
use RS\Services\RS;

/**
* RSInvoice Class
*/
class RSInvoice extends RS
{
	protected $user_id;
	protected $sua;
	
	function __construct()
	{
		parent::__construct();
		$this->uri = Config::INVOICE_URI;
        $option = array("trace"=>true);
		$this->client = new \SoapClient($this->uri, $option);

		$this->check();
		$this->getUNID();
	}

	public function check()
	{
		$response = $this->client->chek([
			'su' => $this->su,
			'sp' => $this->sp,
			'user_id' => $this->tin
		]);

		if(!$response->chekResult)
			throw new Exception("Invalid user, check not successful");

		$this->user_id = $response->user_id;
		$this->sua = $response->sua;
		return true;
	}

	public function getUNID()
	{
		$response = $this->client->get_un_id_from_tin([
			'user_id' => $this->user_id,
			'tin' => $this->tin,
			'su' => $this->su,
			'sp' => $this->sp,
		]);

		if(!$response->get_un_id_from_tinResult)
			throw new Exception("UN ID ERROR");

		$this->un_id = $response->get_un_id_from_tinResult;

		return true;
	}

	public function getBuyerInvoices($startDate='2016-03-28T12:15:21', $endDate='2017-03-28T12:15:21')
	{
		$_start = new \DateTime($startDate);
		$_end = new \DateTime($endDate);

		$_start = $_start->format('c');
		$_end = $_end->format('c');

		$response = $this->client->get_buyer_invoices([
			'su' => $this->su,
			'sp' => $this->sp,
			'user_id' => $this->user_id,
			'un_id' => $this->un_id,
			's_dt' => $_start,
			'e_dt' => $_end,
			'op_s_dt' => $_start,
			'op_e_dt' => $_end,
			'invoice_no' => '',
			'sa_ident_no' => '',
			'desc' => '',
			'doc_mos_nom' => ''
		]);

		// return $this->client->__getLastResponse();
		
		return $this->parseResult($this->client->__getLastResponse());
	}

	public function parseResult($value='')
	{
		$step1 = explode('diffgr:diffgram', $value)[1];
		$parsed = '<diffgr:diffgram' . $step1 . 'diffgr:diffgram>';
		$load = simplexml_load_string($parsed);

		$load = json_decode(json_encode($load));

		return $load->DocumentElement;
	}

}