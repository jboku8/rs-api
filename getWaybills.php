<?php

/**
 * @package RS waybills
 * 
 * Get waybills from RS and insert/update finnished ones in Database
 * @author Giorgi Bagdavadze <notgiorgi@gmail.com>
 * Effort for us, Starry
 */


require 'vendor/autoload.php';

$wbService = new RS\Services\RSWaybill();

$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
$logger->info("Waybills Job run start");

try{
	
	$DB = new RS\Models\Database();
	if(!$DB){
		throw new Exception("Error connecting to DB");
	}

	$data = $wbService->getBuyerWaybills();

	if(isset($data->WAYBILL)){
		$data = $data->WAYBILL;
	}else{
		throw new Exception("No data was returned");
	}

	$waybills = array();

	foreach ($data as $key => $wb) {
		if($wb->STATUS == "2"){

			$tmp = new RS\Models\Waybill($wb);
			
			$res = $DB->save($tmp);
			if(!$res){
				throw new Exception("Error saving data to db");
			}
			array_push($waybills, $tmp);
		}
	}

	$logger->info(count($waybills) . " Waybills fetched");
}
catch(Exception $ex){
	$logger->error($ex->getMessage());
	echo $ex->getMessage();
}
finally{
	$DB->close();
	$logger->info('Waybills Job run end');
}